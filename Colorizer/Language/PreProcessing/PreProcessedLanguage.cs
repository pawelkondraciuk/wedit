﻿namespace CodeColorizer.Language.PreProcessing
{
    using System.Collections.Generic;
    using System.Windows.Documents;

    internal class PreProcessedLanguage : IPreProcessedLanguage
    {
        private readonly ILanguage language;

        private readonly IPreProcessedRules preProcessedRules;

        internal PreProcessedLanguage(FlowDocument sourceCode, ILanguage language)
        {
            this.language = language;
            this.SourceCode = sourceCode;
            this.preProcessedRules = new PreProcessedRules(new TextRange(sourceCode.ContentStart, sourceCode.ContentEnd).Text, this.GetRules());
        }

        public IPreProcessedRules GetPreProcessedRules()
        {
            return this.preProcessedRules;
        }

        public IEnumerable<Rule> GetRules()
        {
            return this.language.GetRules();
        }

        public FlowDocument SourceCode
        {
            get;
            set;
        }
    }
}
