﻿namespace CodeColorizer.Language.PreProcessing
{
    internal interface IPreProcessedLanguage : ILanguage
    {
        System.Windows.Documents.FlowDocument SourceCode { get; }

        IPreProcessedRules GetPreProcessedRules();
    }
}
