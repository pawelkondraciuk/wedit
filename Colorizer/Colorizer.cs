﻿namespace CodeColorizer
{
    public static class Colorizer
    {
        public static ICodeColorizer Colorize(System.Windows.Documents.FlowDocument sourceCode)
        {
            return new CodeColorizer(sourceCode);
        }
    }
}
