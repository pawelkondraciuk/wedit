﻿namespace CodeColorizer.Parsers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using global::CodeColorizer.Language.PreProcessing;
    using global::CodeColorizer.Theme;
    using System.Windows.Documents;
    using System;

    internal class Worker
    {
        private readonly IPreProcessedLanguage preProcessedLanguage;

        private readonly ITheme theme;

        internal Worker(IPreProcessedLanguage preProcessedLanguage, ITheme theme)
        {
            this.preProcessedLanguage = preProcessedLanguage;
            this.theme = theme;
        }

        public void DoWork()
        {
            this.ApplyDocumentStyle(this.theme.BaseHexColor, this.theme.BackgroundHexColor);
            this.ParseSourceCode();
        }

        private FlowDocument ParseSourceCode()
        {
            var source = this.preProcessedLanguage.SourceCode;
            var processedText = new StringBuilder();
            var rules = this.preProcessedLanguage.GetPreProcessedRules().ToList();

            if (!rules.Any())
            {
                return source;
            }

            rules.Sort((emp1, emp2) => emp1.StartIndex.CompareTo(emp2.StartIndex));

            TextRange documentRange = new TextRange(source.ContentStart, source.ContentEnd);
            documentRange.ClearAllProperties();

            var start = source.ContentStart;
            
            int idx = 0;
            for (var i = 0; i < rules.Count(); i++)
            {
                var rule = rules[i];
                var style = this.theme.GetStyle(rule.Scope);

                String txt = new TextRange(start, source.ContentEnd).Text;

                TextPointer wordStart = GetPoint(start, rule.StartIndex - idx);
                TextPointer wordEnd = GetPoint(wordStart, rule.Length);
                
                TextRange range = new TextRange(wordStart, wordEnd);

                FormatSourcePiece(range, style);

                idx = rule.StartIndex + rule.Length;

                start = wordEnd;
            }

            return source;
        }

        private static void FormatSourcePiece(TextRange range, Style style)
        {
            if (style.Bold)
            {
                range.ApplyPropertyValue(TextElement.FontWeightProperty, System.Windows.FontWeights.Bold);
            }

            if (style.Italic)
            {
                range.ApplyPropertyValue(TextElement.FontWeightProperty, System.Windows.FontWeights.Bold);
            }

            range.ApplyPropertyValue(TextElement.ForegroundProperty, new System.Windows.Media.BrushConverter().ConvertFrom(style.HexColor));
        }


        private void ApplyDocumentStyle(string hexColor, string hexBackgroundColor)
        {
            var source = this.preProcessedLanguage.SourceCode;

            source.Background = (System.Windows.Media.Brush)new System.Windows.Media.BrushConverter().ConvertFrom(hexBackgroundColor);
            source.Foreground = (System.Windows.Media.Brush)new System.Windows.Media.BrushConverter().ConvertFrom(hexColor);
        }

        private static TextPointer GetPoint(TextPointer start, int x)
        {
            var ret = start;
            var i = 0;
            while (ret != null)
            {
                string stringSoFar = new TextRange(ret, ret.GetPositionAtOffset(i, LogicalDirection.Forward)).Text;
                if (stringSoFar.Length == x)
                    break;
                i++;
                if (ret.GetPositionAtOffset(i, LogicalDirection.Forward) == null)
                    return ret.GetPositionAtOffset(i - 1, LogicalDirection.Forward);

            }
            ret = ret.GetPositionAtOffset(i, LogicalDirection.Forward);
            return ret;
        }
    }
}
