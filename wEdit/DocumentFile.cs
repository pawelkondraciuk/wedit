﻿using CodeColorizer.Language;
using CodeColorizer.Theme;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace wEdit
{
    public class DocumentFile : INotifyPropertyChanged
    {
        public delegate void FileWatcherChanged(DocumentFile source);
        public event FileWatcherChanged OnFileChanged;

        public FileSystemWatcher watcher = new FileSystemWatcher();
        private Boolean fileExists;

        public Boolean FileExists
        {
            get { return fileExists; }
            set { fileExists = value; }
        }

        private String fileName;

        private String fullPath;

        public String FullPath
        {
            get { return fullPath; }
            set { fullPath = value; fileExists = true; fileName = Path.GetFileName(fullPath); RaisePropertyChanged("Title"); }
        }

        private Boolean changed;

        public Boolean Changed
        {
            get { return changed; }
            set { changed = value; RaisePropertyChanged("Title"); }
        }

        public String Title
        {
            get 
            {
                if (changed)
                    return "*" + fileName;
                return fileName;
            }
        }

        private FlowDocument flow;

        public FlowDocument Flow
        {
            get { if (flow == null) flow = new FlowDocument(); return flow; }
            set 
            { 
                flow = value; 
                RaisePropertyChanged("Flow");
                if (load)
                    load = false;
                else
                    Changed = true;
            }
        }

        private String doc;

        public String Doc
        {
            get { return new TextRange(Flow.ContentStart, Flow.ContentEnd).Text; }
            set 
            {
                new TextRange(Flow.ContentStart, Flow.ContentEnd).Text = value;
                RaisePropertyChanged("Flow");
            }
        }

        private ILanguage language;

        public ILanguage Language
        {
            get { return language; }
            set { language = value; RaisePropertyChanged("Language"); }
        }
        private static ITheme theme;

        public static ITheme Theme
        {
            get { return theme; }
            set { theme = value; }
        }

        Boolean load = true;

        public DocumentFile()
        {
            Doc = String.Empty;
            Changed = false;
            Language = null;

            fileName = "Nowy plik";
        }

        public DocumentFile(String filePath, String content)
        {
            FullPath = filePath;
            Doc = content;
            Changed = false;
            Language = null;
            fileExists = true;

            watcher.Path = Path.GetDirectoryName(filePath);
            watcher.Filter = Path.GetFileName(filePath);
            watcher.EnableRaisingEvents = true;
            watcher.Changed += watcher_Changed;
        }

        void watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (OnFileChanged != null)
            {
                OnFileChanged(this);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }
    }
}
