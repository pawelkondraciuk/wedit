﻿using CodeColorizer.Theme;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace wEdit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Engine engine;

        FindAndReplace findWindow;

        FontChooser fontChooser;

        IniFile prop_ini;


        public MainWindow()
        {
            engine = new Engine();
            InitializeComponent();
            DataContext = engine;
        }

        /// <summary>
        /// Hide arrow marker on the Toolbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolBar_Loaded(object sender, RoutedEventArgs e)
        {
            ToolBar toolBar = sender as ToolBar;
            var overflowGrid = toolBar.Template.FindName("OverflowGrid", toolBar) as FrameworkElement;
            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }

            var mainPanelBorder = toolBar.Template.FindName("MainPanelBorder", toolBar) as FrameworkElement;
            if (mainPanelBorder != null)
            {
                mainPanelBorder.Margin = new Thickness(0);
            }
        }

        /// <summary>
        /// Add indentation with tab button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RichTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Tab)
                return;

            var richTextBox = sender as RichTextBox;
            if (richTextBox == null) return;

            if (richTextBox.Selection.Text != string.Empty)
                richTextBox.Selection.Text = string.Empty;

            var caretPosition = richTextBox.CaretPosition.GetPositionAtOffset(0,
                                    LogicalDirection.Forward);

            richTextBox.CaretPosition.InsertTextInRun("\t");
            richTextBox.CaretPosition = caretPosition;
            e.Handled = true;
        }

        /// <summary>
        /// Retrieves RichTextBox from DataTemplate
        /// </summary>
        /// <returns></returns>
        private RichTextBox GetCurrentRichTextBox()
        {
            ContentPresenter cp = tControl.Template.FindName("PART_SelectedContentHost", tControl) as ContentPresenter;
            if (cp != null)
                return tControl.ContentTemplate.FindName("RTB", cp) as RichTextBox;
            return null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            prop_ini = new IniFile("properties.ini");

            #region loading previous session
            if (prop_ini.KeyExists("selected_tab", "session"))
            {

                int selected_id = 0;
                DocumentFile selected = null;
                try { selected_id = Convert.ToInt32(prop_ini.Read("selected_tab", "session")); }
                catch (Exception ex) { Console.WriteLine("Error in properties.ini occured!"); }


                int i = 0;
                while (prop_ini.KeyExists("file" + i, "session"))
                {
                    string path = prop_ini.Read("file" + i, "session");
                    bool added = false;
                    if (File.Exists(@path))
                    {
                            engine.OpenFile(path);
                            added = true;
                    }
                    else if (i == selected_id) selected_id = 0;


                    if (added)
                    {
                        if (i == selected_id) selected = engine.SelectedTab;//lastfile;

                        string filelang = prop_ini.Read("lang" + i, "session");
                        if (filelang != null)
                        {

                            if (!filelang.Equals("-null-"))
                            {
                                foreach (CodeColorizer.Language.ILanguage lang in engine.LanguageList)
                                {
                                        if (lang != null) if (lang.ToString().Equals(filelang))
                                        {
                                            engine.SelectedTab.Language = lang;
                                            break;
                                        }

                                }
                            }

                        }

                    }


                    i++;
                }

                foreach (DocumentFile file in engine.FileList) { if (engine.FileList.Count > 1) engine.Close(file); break;}
                foreach (DocumentFile file in engine.FileList)
                {
                    if (selected != null) engine.SelectedTab = selected;
                    else engine.SelectedTab = file;
                    break;
                }

            }
            #endregion

            #region loading_settings

            string themename = prop_ini.Read("theme", "settings");
            if (themename != null)
            {

                foreach (ITheme theme in engine.ThemeList)
                {

                    if (theme != null) if (theme.ToString().Equals(themename))
                        {
                            engine.SelectedTheme = theme;
                            break;
                        }

                }
            }


            if (prop_ini.KeyExists("font_family", "settings")) engine.FontFamily = new FontFamily(prop_ini.Read("font_family", "settings"));

            //string fonttypeface_str = prop_ini.Read("font_typeface_index", "settings");
            if (prop_ini.KeyExists("font_typeface_index", "settings"))
            {
                int fonttypeface;
                try
                {
                    fonttypeface = Convert.ToInt32(prop_ini.Read("font_typeface_index", "settings"));
                    engine.FontTypefaceIndex = fonttypeface;
                }
                catch (Exception ex) { Console.WriteLine("Error in properties.ini occured!"); }

            }

            //string fontsize_str = prop_ini.Read("font_size", "settings");
            if (prop_ini.KeyExists("font_size", "settings"))
            {
                int fontsize;
                try
                {
                    fontsize = Convert.ToInt32(prop_ini.Read("font_size", "settings"));
                    engine.FontSize = fontsize;
                }
                catch (Exception ex) { Console.WriteLine("Error in properties.ini occured!"); }

            }

            if (prop_ini.KeyExists("text_wrap", "settings")) engine.TextWrap = Convert.ToBoolean(prop_ini.Read("text_wrap", "settings"));
            if (prop_ini.KeyExists("auto_indents", "settings")) engine.AutoIndents = Convert.ToBoolean(prop_ini.Read("auto_indents", "settings"));

            #endregion
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            // Zapisywanie wszystkich plikow
            if (engine.SaveAll() == Engine.CloseResult.CANCELED)
            {
                e.Cancel = true;
                return;
            }
            prop_ini.DeleteSection("session");

            //saving session
            int i = 0, selected = 0;
            foreach (DocumentFile file in engine.FileList)
            {
                if (file.FileExists)
                {
                    string path = file.FullPath, lang = "-null-";
                    if (path == null) path = "-null-";
                    else if (file.Language != null) lang = file.Language.ToString();
                    prop_ini.Write("file" + i, path, "session");
                    prop_ini.Write("lang" + i, lang, "session");

                    if (engine.SelectedTab == file)
                    {
                        prop_ini.Write("selected_tab", i + "", "session");
                    }

                    i++;
                }
            }


            //saving settings
            string themename = null;
            if (engine.SelectedTheme != null) themename = engine.SelectedTheme.ToString();
            prop_ini.Write("theme", themename, "settings");

            prop_ini.Write("font_family", engine.FontFamily.ToString(), "settings");
            prop_ini.Write("font_typeface_index", engine.FontTypefaceIndex + "", "settings");
            prop_ini.Write("font_size", engine.FontSize + "", "settings");

            prop_ini.Write("text_wrap", engine.TextWrap.ToString(), "settings");
            prop_ini.Write("auto_indents", engine.AutoIndents.ToString(), "settings");


        }

        private void NewFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            engine.NewFile();
        }

        private void OpenFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = true;

            bool? userClickedOK = openFileDialog1.ShowDialog();

            if (userClickedOK == true)
            {
                engine.OpenFile(openFileDialog1.FileName);
            }
        }

        private void SaveFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            engine.SaveFile(engine.SelectedTab);
        }

        private void DirectoryList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext as String;
            engine.OpenFile(Path.GetDirectoryName(engine.SelectedTab.FullPath) + Path.DirectorySeparatorChar + item);
        }

        private void tControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            engine.RefreshDirectoryList();
        }

        private void CloseContext_Clicked(object sender, RoutedEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext as DocumentFile;

            engine.Close(item);
        }

        private void CloseAllButThisContext_Clicked(object sender, RoutedEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext as DocumentFile;

            engine.CloseAllButThis(item);
        }

        private void CloseAll_Clicked(object sender, RoutedEventArgs e)
        {
            engine.CloseAll();
        }

        private void ThemeMenu_Clicked(object sender, RoutedEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext as CodeColorizer.Theme.ITheme;
            engine.SelectedTheme = item;
        }

        private void LanguageMenu_Clicked(object sender, RoutedEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext as CodeColorizer.Language.ILanguage;
            engine.SelectedTab.Language = item;
        }

        private void SaveAsFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            engine.SaveAsFile(engine.SelectedTab);
        }

        private void CloseApp_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void PrintFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FlowDocument document = GetCurrentRichTextBox().Document;
            System.IO.MemoryStream s = new System.IO.MemoryStream();
            TextRange source = new TextRange(document.ContentStart, document.ContentEnd);
            source.Save(s, DataFormats.Xaml);
            FlowDocument copy = new FlowDocument();
            TextRange dest = new TextRange(copy.ContentStart, copy.ContentEnd);
            dest.Load(s, DataFormats.Xaml);

            System.Printing.PrintDocumentImageableArea ia = null;
            System.Windows.Xps.XpsDocumentWriter docWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter(ref ia);

            if (docWriter != null && ia != null)
            {
                DocumentPaginator paginator = ((IDocumentPaginatorSource)copy).DocumentPaginator;

                paginator.PageSize = new Size(ia.MediaSizeWidth, ia.MediaSizeHeight);
                Thickness t = new Thickness(72);
                copy.PagePadding = new Thickness(
                                 Math.Max(ia.OriginWidth, t.Left),
                                   Math.Max(ia.OriginHeight, t.Top),
                                   Math.Max(ia.MediaSizeWidth - (ia.OriginWidth + ia.ExtentWidth), t.Right),
                                   Math.Max(ia.MediaSizeHeight - (ia.OriginHeight + ia.ExtentHeight), t.Bottom));

                copy.ColumnWidth = double.PositiveInfinity;

                docWriter.Write(paginator);
            }
        }
        private void OpenFindWindow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            String param = (String)e.Parameter;
            if (findWindow == null || !findWindow.IsVisible)
            {
                ContentPresenter cp = tControl.Template.FindName("PART_SelectedContentHost", tControl) as ContentPresenter;
                SyntaxRTB rtb = tControl.ContentTemplate.FindName("RTB", cp) as SyntaxRTB;
                findWindow = new FindAndReplace(rtb);

            }

            if (param == "Find")
                findWindow.SelectedIndex = 0;
            else if (param == "Replace")
                findWindow.SelectedIndex = 1;
            else
                findWindow.SelectedIndex = 2;

            findWindow.Show();
        }

        private void SaveAll_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            engine.SaveAll();
        }


        private void OpenFontChooser_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            if (fontChooser == null || !fontChooser.IsVisible)
            {
                fontChooser = new FontChooser(engine);
                fontChooser.Show();
            }

        }

        private void InvertFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Console.WriteLine("odwracanie pliku");
            String text = engine.SelectedTab.Doc;

            String[] lines = text.Split(new string[] { "\r\n", "\n", "\r" }, StringSplitOptions.None);

            Array.Reverse(lines);

            engine.SelectedTab.Doc = String.Join("\r\n", lines);

        }

    }
}
