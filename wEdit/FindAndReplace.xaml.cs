﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace wEdit
{
    /// <summary>
    /// Interaction logic for FindAndReplace.xaml
    /// </summary>
    public partial class FindAndReplace : Window, INotifyPropertyChanged, IDataErrorInfo
    {
        #region Private Fields

        SyntaxRTB rtb;
        FindAndReplaceManager manager;
        FindOptions findOptions;
        ObservableCollection<Pair<String, Scope>> scopeList = new ObservableCollection<Pair<String, Scope>>();
        Pair<String, Scope> selectedScope;
        String findString;
        String replaceString;
        int selectedIndex = 0;
        int lineNumber = 0;
        #endregion

        #region Getters & Setters

        public ObservableCollection<Pair<String, Scope>> ScopeList
        {
            get { return scopeList; }
            set { scopeList = value; }
        }

        public Pair<String, Scope> SelectedScope
        {
            get { return selectedScope; }
            set 
            { 
                selectedScope = value; 
                RaisePropertyChanged("SelectedScope");
                if(selectedScope.Second == Scope.SELECTION)
                    manager = new FindAndReplaceManager(rtb.Selection.Start, rtb.Selection.End);
                else
                    manager = new FindAndReplaceManager(rtb.Document.ContentStart, rtb.Document.ContentEnd);
            }
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }
            set { selectedIndex = value; }
        }

        public String FindString
        {
            get { return findString; }
            set { findString = value; RaisePropertyChanged("FindString"); }
        }

        public String ReplaceString
        {
            get { return replaceString; }
            set { replaceString = value; RaisePropertyChanged("ReplaceString"); }
        }

        private void SetOption(FindOptions option, Boolean value)
        {
            if (value)
            {
                findOptions |= option;
            }
            else
            {
                findOptions &= ~option;
            }
        }

        private Boolean GetOption(FindOptions option)
        {
            return (findOptions & option) != 0;
        }

        public Boolean MatchCase
        {
            get { return GetOption(FindOptions.MatchCase); }
            set
            {
                SetOption(FindOptions.MatchCase, value);
                RaisePropertyChanged("MatchCase");
            }
        }

        public Boolean MatchWholeWord
        {
            get { return GetOption(FindOptions.MatchWholeWord); }
            set
            {
                SetOption(FindOptions.MatchWholeWord, value);
                RaisePropertyChanged("MatchWholeWord");
            }
        }

        public int LineNumber
        {
            get { return lineNumber; }
            set { lineNumber = value; }
        }

        #endregion

        #region Ctor

        public FindAndReplace(SyntaxRTB rtb)
        {
            InitializeComponent();
            DataContext = this;
            this.rtb = rtb;

            manager = new FindAndReplaceManager(rtb.Document.ContentStart, rtb.Document.ContentEnd);

            scopeList.Add(selectedScope = new Pair<string, Scope>("Bierzący dokument", Scope.CURRENT_DOCUMENT));
            scopeList.Add(new Pair<string, Scope>("Zaznaczenie", Scope.SELECTION));
        }

        #endregion

        #region Handlers

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Find_Click(object sender, RoutedEventArgs e)
        {
            FindAndSelect();
        }

        private void Replace_Click(object sender, RoutedEventArgs e)
        {
            ReplaceAndSelect();
        }
        
        private void ReplaceAll_Click(object sender, RoutedEventArgs e)
        {
            ReplaceAll();
        }

        private void GoToLine_Click(object sender, RoutedEventArgs e)
        {
            Control ctrl = sender as Control;
            if (IsValid(this))
            {
                GoToLine();
            }

        }

        #endregion

        #region Private Methods

        private void FindAndSelect()
        {
            TextRange textRange = manager.FindNext(FindString, findOptions);
            if (textRange != null)
            {
                rtb.Focus();
                rtb.Selection.Select(textRange.Start, textRange.End);
            }
            else
            {
                if (manager.CurrentPosition.CompareTo(rtb.Document.ContentEnd) == 0)
                {
                    MessageBox.Show("Dotarłeś do końca dokumentu!");
                    rtb.CaretPosition = rtb.Document.ContentStart;
                    manager.CurrentPosition = rtb.CaretPosition;
                }
            }
        }

        private void ReplaceAndSelect()
        {
            TextRange textRange = manager.Replace(FindString, ReplaceString, findOptions);
            if (textRange != null)
            {
                rtb.Focus();
                rtb.Selection.Select(textRange.Start, textRange.End);
            }
            else
            {
                if (manager.CurrentPosition.CompareTo(rtb.Document.ContentEnd) == 0)
                {
                    MessageBox.Show("Dotarłeś do końca dokumentu!");
                    rtb.CaretPosition = rtb.Document.ContentStart;
                    manager.CurrentPosition = rtb.CaretPosition;
                }
            }
        }

        private void ReplaceAll()
        {
            MessageBox.Show(String.Format(
                "{0} wystąpień zostało zamienionych.",
                manager.ReplaceAll(FindString, ReplaceString, findOptions)));
        }

        private void GoToLine()
        {
            int currentLine = 0;

            TextPointer navigator = rtb.Document.ContentStart;

            while (navigator.CompareTo(rtb.Document.ContentEnd) < 0)
            {
                TextPointerContext context = navigator.GetPointerContext(LogicalDirection.Backward);

                if (context == TextPointerContext.ElementStart && navigator.Parent is Paragraph)
                {
                    currentLine++;
                    if (currentLine == lineNumber)
                        break;
                }

                navigator = navigator.GetNextContextPosition(LogicalDirection.Forward);
            }

            Close();

            if (navigator == null)
                rtb.CaretPosition = rtb.Document.ContentEnd;
            else
                rtb.CaretPosition = navigator;
            rtb.Focus();
        }

        private bool IsValid(DependencyObject parent)
        {
            // The dependency object is valid if it has no errors, 
            //and all of its children (that are dependency objects) are error-free.
            if (Validation.GetHasError(parent))
                return false;

            for (int i = 0; i != VisualTreeHelper.GetChildrenCount(parent); ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (!IsValid(child)) { return false; }
            }

            return true;
        }

        #endregion

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        #region IDataErrorInfo Implementation

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                if (columnName == "LineNumber")
                {
                    if(LineNumber < 1)
                        result = "Podaj poprawną liczbę";
                }
                return result;
            }
        }
        #endregion

    }

    public enum Scope
    {
        CURRENT_DOCUMENT,
        SELECTION
    }

    public class Pair<T, U>
    {
        public Pair()
        {
        }

        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }

        public T First { get; set; }
        public U Second { get; set; }
    };
}
