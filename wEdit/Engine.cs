﻿using CodeColorizer.Language;
using CodeColorizer.Theme;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace wEdit
{
    class Engine : INotifyPropertyChanged
    {
        public enum CloseResult { CLOSED, CANCELED };

        ObservableCollection<ITheme> themeList = new ObservableCollection<ITheme>();
        ITheme selectedTheme;
        ObservableCollection<ILanguage> languageList = new ObservableCollection<ILanguage>();
        ObservableCollection<DocumentFile> fileList = new ObservableCollection<DocumentFile>();
        DocumentFile selectedTab;
        ObservableCollection<String> filesInDirectory = new ObservableCollection<String>();

        Boolean textWrap = false;
        Boolean autoIndents = true;

        public const string DEFAULT_FONT_NAME = "Calibri";
        FontFamily fontFamily = new FontFamily(DEFAULT_FONT_NAME);
        Typeface fontTypeface = new Typeface(DEFAULT_FONT_NAME);
        int fontTypefaceIndex = -1;
        int fontSize = 12;


        public event PropertyChangedEventHandler PropertyChanged;

        public Engine()
        {
            PrepareThemeList();
            PrepareLanguageList();
            if (fileList.Count == 0)
                NewFile();
        }

        public ObservableCollection<String> FilesInDirectory
        {
            get { return filesInDirectory; }
            set { filesInDirectory = value; }
        }
        public DocumentFile SelectedTab
        {
            get { return selectedTab; }
            set
            {
                selectedTab = value;
                RaisePropertyChanged("SelectedTab");
            }
        }
        public ObservableCollection<DocumentFile> FileList
        {
            get { return fileList; }
            set { fileList = value; }
        }
        public ObservableCollection<ITheme> ThemeList
        {
            get { return themeList; }
            set { themeList = value; }
        }
        public ITheme SelectedTheme
        {
            get { return selectedTheme; }
            set { selectedTheme = value; RaisePropertyChanged("SelectedTheme"); }
        }
        public FontFamily FontFamily
        {
            get { return fontFamily; }
            set
            {
                fontFamily = value;
                FontTypeface = new Typeface(fontFamily.ToString());
                FontTypefaceIndex = -1;
                RaisePropertyChanged("FontFamily");
            }
        }
        public Typeface FontTypeface
        {
            get { return fontTypeface; }
            set { fontTypeface = value; RaisePropertyChanged("FontTypeface"); }
        }
        public int FontTypefaceIndex
        {
            get { return fontTypefaceIndex; }
            set
            {
                fontTypefaceIndex = value;

                int i = 0;
                foreach (Typeface tface in FontFamily.GetTypefaces())
                {
                    if (FontTypefaceIndex == i)
                    {
                        FontTypeface = tface;
                        break;
                    }
                    i++;
                }

                RaisePropertyChanged("FontTypefaceIndex");
            }
        }
        public int FontSize
        {
            get { return fontSize; }
            set { fontSize = value; RaisePropertyChanged("FontSize"); }
        }

        public ObservableCollection<ILanguage> LanguageList
        {
            get { return languageList; }
            set { languageList = value; }
        }

        public Boolean AutoIndents
        {
            get { return autoIndents; }
            set { autoIndents = value; RaisePropertyChanged("AutoIndents"); }
        }

        public Boolean TextWrap
        {
            get { return textWrap; }
            set { textWrap = value; RaisePropertyChanged("TextWrap"); }
        }


        protected void RaisePropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(name));
        }

        private void PrepareThemeList()
        {
            ThemeList.Add(null);
            ThemeList.Add(new CodeColorizer.Theme.Themes.ObsidianTheme());
        }

        private void PrepareLanguageList()
        {
            LanguageList.Add(null);
            LanguageList.Add(new CodeColorizer.Language.Languages.Csharp());
        }

        public void NewFile()
        {
            DocumentFile file = new DocumentFile();
            fileList.Add(file);
            SelectedTab = file;
        }

        public void OpenFile(String filepath)
        {
            if (!FileListContains(filepath))
            {
                String content = File.ReadAllText(filepath, Encoding.Default);
                DocumentFile file = new DocumentFile(filepath, content);
                file.OnFileChanged += file_OnFileChanged;
                fileList.Add(file);
                SelectedTab = file;
            }
        }

        public CloseResult SaveAsFile(DocumentFile file)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;

            bool? userClickedOK = saveFileDialog.ShowDialog();

            if (userClickedOK == true)
            {
                String path = saveFileDialog.FileName;
                file.watcher.EnableRaisingEvents = false;
                File.WriteAllText(path, file.Doc);
                file.watcher.EnableRaisingEvents = true;
                file.FullPath = path;
                file.Changed = false;

                return CloseResult.CLOSED;
            }

            return CloseResult.CANCELED;
        }

        public CloseResult SaveFile(DocumentFile file)
        {
            if (file == null)
                return CloseResult.CANCELED;

            if (file.FileExists)
            {
                file.watcher.EnableRaisingEvents = false;
                File.WriteAllText(file.FullPath, file.Doc);
                file.watcher.EnableRaisingEvents = true;
                file.Changed = false;

                return CloseResult.CLOSED;
            }
            else
            {
                return SaveAsFile(file);
            }

            return CloseResult.CANCELED;
        }

        void file_OnFileChanged(DocumentFile source)
        {
            if (MessageBox.Show(String.Format("Wykryto zmianę w pliku: {0}\nCzy przeładować plik?", source.FullPath), "Wykryto zmianę", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                source.Doc = File.ReadAllText(source.FullPath, Encoding.Default);
            }
        }

        private Boolean FileListContains(String filepath)
        {
            foreach (DocumentFile i in fileList)
            {
                if (i.FileExists && i.FullPath.Equals(filepath))
                    return true;
            }

            return false;
        }

        public void RefreshDirectoryList()
        {
            if (selectedTab != null && selectedTab.FileExists)
            {
                FilesInDirectory.Clear();
                string[] files = Directory.GetFiles(Path.GetDirectoryName(selectedTab.FullPath));
                foreach (var i in files)
                {
                    FilesInDirectory.Add(Path.GetFileName(i));
                }
            }
            else
                FilesInDirectory.Clear();
        }

        public CloseResult PromptAndSave(DocumentFile file)
        {
            CloseResult ret = CloseResult.CLOSED;

            if (file.Changed)
            {
                SelectedTab = file;
                MessageBoxResult result = MessageBox.Show(String.Format("Czy zapisać plik {0}?", file.FullPath), "Zapis", MessageBoxButton.YesNoCancel);

                if (result == MessageBoxResult.Yes)
                {
                    ret = SaveFile(file);
                }
                else if (result == MessageBoxResult.No)
                {
                    ret = CloseResult.CLOSED;
                }
                else
                {
                    ret = CloseResult.CANCELED;
                }
            }

            return ret;
        }

        public CloseResult Close(DocumentFile file)
        {
            CloseResult ret = PromptAndSave(file);

            if (FileList.Count == 1)
                NewFile();
            FileList.Remove(file);

            return ret;
        }

        public CloseResult CloseAllButThis(DocumentFile file)
        {
            List<DocumentFile> cpy = FileList.ToList<DocumentFile>();
            foreach (DocumentFile f in cpy)
            {
                if (f != file)
                    if (Close(f) == CloseResult.CANCELED)
                        return CloseResult.CANCELED;
            }
            SelectedTab = file;

            return CloseResult.CLOSED;
        }

        public CloseResult CloseAll()
        {
            List<DocumentFile> cpy = FileList.ToList<DocumentFile>();
            foreach (DocumentFile f in cpy)
            {
                if (Close(f) == CloseResult.CANCELED)
                    return CloseResult.CANCELED;
            }
            return CloseResult.CLOSED;
        }

        public CloseResult SaveAll()
        {
            foreach (DocumentFile f in FileList)
                if (PromptAndSave(f) == CloseResult.CANCELED)
                    return CloseResult.CANCELED;
            return CloseResult.CLOSED;
        }
    }
}