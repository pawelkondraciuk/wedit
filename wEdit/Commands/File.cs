﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace wEdit.Commands
{
    public class File
    {
        private static RoutedUICommand newFile;
        private static RoutedUICommand openFile;
        private static RoutedUICommand saveFile;
        private static RoutedUICommand saveAsFile;
        private static RoutedUICommand printFile;
        private static RoutedUICommand saveAll;

        public static RoutedUICommand SaveAll
        {
            get { return File.saveAll; }
            set { File.saveAll = value; }
        }

        public static RoutedUICommand NewFile
        {
            get { return newFile; }
        }

        public static RoutedUICommand PrintFile
        {
            get { return printFile; }
        }

        public static RoutedUICommand OpenFile
        {
            get { return openFile; }
        }

        public static RoutedUICommand SaveFile
        {
            get { return saveFile; }
        }

        public static RoutedUICommand SaveAsFile
        {
            get { return saveAsFile; }
        }

        static File()
        {
            newFile = new RoutedUICommand("Create new file", "NewFile", typeof(File));
            openFile = new RoutedUICommand("Open file", "OpenFile", typeof(File));
            saveFile = new RoutedUICommand("Save file", "SaveFile", typeof(File));
            saveAsFile = new RoutedUICommand("Save file as", "SaveAsFile", typeof(File));
            printFile = new RoutedUICommand("Print file", "PrintFile", typeof(File));
            saveAll = new RoutedUICommand("Save all files", "SaveFile", typeof(File));
        }
    }
}
