﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace wEdit.Commands
{
    public class Misc
    {
        private static RoutedUICommand openFindWindow;
        private static RoutedUICommand openFontChooser;
        private static RoutedUICommand invertFile;

        public static RoutedUICommand OpenFindWindow
        {
            get { return openFindWindow; }
        }

        public static RoutedUICommand OpenFontChooser
        {
            get { return openFontChooser; }
        }

        public static RoutedUICommand InvertFile
        {
            get { return invertFile; }
        }


        static Misc()
        {
            openFindWindow = new RoutedUICommand("Open find widnow", "OpenFindWindow", typeof(Misc));
            openFontChooser = new RoutedUICommand("Open font chooser", "OpenFontChooser", typeof(Misc));
            invertFile = new RoutedUICommand("Inverting opened file", "InvertFile", typeof(Misc));

        }
    }
}
