﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wEdit
{
    /// <summary>
    /// Logika interakcji dla klasy FontChooser.xaml
    /// </summary>
    public partial class FontChooser : Window
    {
        Engine engine;

        public FontChooser(Object engine)
        {
            InitializeComponent();
            DataContext = (Engine)engine;
            this.engine = (Engine)engine;

            //FontFamily f = Fonts.SystemFontFamilies.First();
            //Console.WriteLine("Typ czcionka: "+f.GetType().ToString());
        }

        private void OK_Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
