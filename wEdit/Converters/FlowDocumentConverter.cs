﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup;

namespace wEdit.Converters
{
    class FlowDocumentConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            FlowDocument flowDocument = new FlowDocument();
            if (value != null)
            {
                String input = (string)value;
                using (StringReader reader = new StringReader(input))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        flowDocument.Blocks.Add(new Paragraph(new Run(line)));
                    }
                }
            }
            return flowDocument;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return string.Empty;

            var flowDocument = (FlowDocument)value;

            return new TextRange(flowDocument.ContentStart, flowDocument.ContentEnd).Text;
        }

        #endregion
    }
}