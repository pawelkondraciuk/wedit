﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wEdit.Models
{
    public class RecentFileViewModel
    {
        private int _index;
        private string _fileName;

        public RecentFileViewModel(int index, string fileName)
        {
            _index = index;
            _fileName = fileName;
        }

        public string IndexedFileName
        {
            get { return string.Format("_{0} - {1}", _index + 1, _fileName); }
        }

        public string FileName
        {
            get { return _fileName; }
        }
    }
}
