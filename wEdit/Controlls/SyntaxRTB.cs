using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace wEdit
{
    public class SyntaxRTB : RichTextBox
    {
        //------------------------------------------------------
        //
        //  Constructors
        //
        //------------------------------------------------------

        // Ctor.
        static SyntaxRTB()
        {
            RegisterCommandHandlers();
        }

        static void RegisterCommandHandlers()
        {
            foreach (RoutedUICommand command in _formattingCommands)
            {
                CommandManager.RegisterClassCommandBinding(typeof(SyntaxRTB),
                    new CommandBinding(command, new ExecutedRoutedEventHandler(OnFormattingCommand), 
                    new CanExecuteRoutedEventHandler(OnCanExecuteFormattingCommand)));
            }

            CommandManager.RegisterClassCommandBinding(typeof(SyntaxRTB),
                new CommandBinding(ApplicationCommands.Copy, new ExecutedRoutedEventHandler(OnCopy), 
                new CanExecuteRoutedEventHandler(OnCanExecuteCopy)));
            CommandManager.RegisterClassCommandBinding(typeof(SyntaxRTB),
                new CommandBinding(ApplicationCommands.Paste, new ExecutedRoutedEventHandler(OnPaste), 
                new CanExecuteRoutedEventHandler(OnCanExecutePaste)));
            CommandManager.RegisterClassCommandBinding(typeof(SyntaxRTB),
                new CommandBinding(ApplicationCommands.Cut, new ExecutedRoutedEventHandler(OnCut), 
                new CanExecuteRoutedEventHandler(OnCanExecuteCut)));
        }

        public SyntaxRTB() : base()
        {
            this.TextChanged += this.TextChangedEventHandler;
            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += _timer_Elapsed;

            MyContextMenu myContextMenu = new MyContextMenu(this);
            myContextMenu.Placement = PlacementMode.RelativePoint;
            myContextMenu.PlacementTarget = this;

            this.ContextMenu = myContextMenu;
        }


        void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Stop();
            this.Dispatcher.Invoke((Action)delegate()
            {
                UpdateStyle();
            });
        }

        public void UpdateStyle()
        {
            this.TextChanged -= this.TextChangedEventHandler;
            TextRange documentRange = new TextRange(this.Document.ContentStart, this.Document.ContentEnd);
            documentRange.ClearAllProperties();
            if (theme == null)
            {
                if (Document.Background != null || Document.Foreground != Brushes.Black)
                {
                    Document.Background = null;
                    Document.Foreground = Brushes.Black;
                    this.CaretBrush = Document.Foreground;
                }
            }
            else
            {
                Document.Background = (System.Windows.Media.Brush)new System.Windows.Media.BrushConverter().ConvertFrom(theme.BackgroundHexColor);
                Document.Foreground = (System.Windows.Media.Brush)new System.Windows.Media.BrushConverter().ConvertFrom(theme.BaseHexColor);
                this.CaretBrush = Document.Foreground;

                if (lang != null)
                {
                    CodeColorizer.Colorizer.Colorize(Document)
                    .WithLanguage(lang)
                    .WithTheme(theme)
                    .Execute();
                }
            }
            this.TextChanged += this.TextChangedEventHandler;
            
        }
        #region Event Handlers

        /// <summary>
        /// Event handler for all formatting commands.
        /// </summary>
        private static void OnFormattingCommand(object sender, ExecutedRoutedEventArgs e)
        {
            // Do nothing, and set command handled to true.
            e.Handled = true;
        }

        /// <summary>
        /// Event handler for ApplicationCommands.Copy command.
        /// <remarks>
        /// We want to enforce that data can be set on the clipboard 
        /// only in plain text format from this RichTextBox.
        /// </remarks>
        /// </summary>
        private static void OnCopy(object sender, ExecutedRoutedEventArgs e)
        {
            SyntaxRTB myRichTextBox = (SyntaxRTB)sender;
            string selectionText = myRichTextBox.Selection.Text;
            Clipboard.SetText(selectionText);
            e.Handled = true;
        }

        /// <summary>
        /// Event handler for ApplicationCommands.Cut command.
        /// <remarks>
        /// We want to enforce that data can be set on the clipboard 
        /// only in plain text format from this RichTextBox.
        /// </remarks>
        /// </summary>
        private static void OnCut(object sender, ExecutedRoutedEventArgs e)
        {
            SyntaxRTB myRichTextBox = (SyntaxRTB)sender;
            string selectionText = myRichTextBox.Selection.Text;
            myRichTextBox.Selection.Text = String.Empty;
            Clipboard.SetText(selectionText);
            e.Handled = true;
        }

        /// <summary>
        /// Event handler for ApplicationCommands.Paste command.
        /// <remarks>
        /// We want to allow paste only in plain text format.
        /// </remarks>
        /// </summary>
        private static void OnPaste(object sender, ExecutedRoutedEventArgs e)
        {
            SyntaxRTB myRichTextBox = (SyntaxRTB)sender;

            // Handle paste only if clipboard supports text format.
            if (Clipboard.ContainsText())
            {
                myRichTextBox.Selection.Text = Clipboard.GetText();
                myRichTextBox.Selection.Select(myRichTextBox.Selection.End, myRichTextBox.Selection.End);
            }
            e.Handled = true;
        }

        /// <summary>
        /// CanExecute event handler.
        /// </summary>
        private static void OnCanExecuteFormattingCommand(object target, CanExecuteRoutedEventArgs args)
        {
            args.CanExecute = true;
        }

        /// <summary>
        /// CanExecute event handler for ApplicationCommands.Copy.
        /// </summary>
        private static void OnCanExecuteCopy(object target, CanExecuteRoutedEventArgs args)
        {
            SyntaxRTB myRichTextBox = (SyntaxRTB)target;
            args.CanExecute = myRichTextBox.IsEnabled && !myRichTextBox.Selection.IsEmpty;
        }

        /// <summary>
        /// CanExecute event handler for ApplicationCommands.Cut.
        /// </summary>
        private static void OnCanExecuteCut(object target, CanExecuteRoutedEventArgs args)
        {
            SyntaxRTB myRichTextBox = (SyntaxRTB)target;
            args.CanExecute = myRichTextBox.IsEnabled && !myRichTextBox.IsReadOnly && !myRichTextBox.Selection.IsEmpty;
        }

        /// <summary>
        /// CanExecute event handler for ApplicationCommand.Paste.
        /// </summary>
        private static void OnCanExecutePaste(object target, CanExecuteRoutedEventArgs args)
        {
            SyntaxRTB myRichTextBox = (SyntaxRTB)target;
            args.CanExecute = myRichTextBox.IsEnabled && !myRichTextBox.IsReadOnly && Clipboard.ContainsText();
        }

        /// <summary>
        /// Event handler for RichTextBox.TextChanged event.
        /// </summary>
        private void TextChangedEventHandler(object sender, TextChangedEventArgs e)
        {
            if(autoIndents)
                ManageTabs(e);
            _timer.Start();
        }

        #endregion

        #region Private Methods

        public void setDocument(FlowDocument doc)
        {
            this.TextChanged -= this.TextChangedEventHandler;
            Document = doc;
            this.TextChanged += this.TextChangedEventHandler;
            UpdateStyle();
        }

        /// <summary>
        /// Zarz�dzaj wci�ciami
        /// </summary>
        private void ManageTabs(TextChangedEventArgs e)
        {
            this.TextChanged -= this.TextChangedEventHandler;
            foreach (TextChange tc in e.Changes)
            {
                TextPointer startPointer = Document.ContentStart.GetPositionAtOffset(tc.Offset, LogicalDirection.Forward);
                TextPointer endPointer = Document.ContentStart.GetPositionAtOffset(tc.Offset + tc.AddedLength, LogicalDirection.Backward);
                if (startPointer.Paragraph != null)
                {
                    var newText = new TextRange(startPointer, endPointer);
                    if (newText.Text.StartsWith("\r\n")) // wci�ni�to enter
                    {
                        Run run = (Run)startPointer.Paragraph.Inlines.FirstInline;

                        if (run != null)
                        {
                            int tabs = 0;

                            foreach (Char c in run.Text)
                            {
                                if (c != '\t')
                                    break;
                                tabs++;
                            }

                            if (true && run.Text.Length > 0)
                            {
                                String lastChar = run.Text.Substring(run.Text.Length - 1);
                                if (lastChar == "{")
                                    tabs++;
                            }
                            if (tabs > 0)
                                Document.ContentStart.GetPositionAtOffset(tc.Offset + tc.AddedLength, LogicalDirection.Backward).InsertTextInRun(new String('\t', tabs));
                        }
                    }
                    else if (newText.Text == "}")
                    {
                        Run run = (Run)startPointer.Paragraph.Inlines.FirstInline;

                        Paragraph previousParagraph = (Paragraph)startPointer.Paragraph.PreviousBlock;
                        Run previousRun = null;
                        if (previousParagraph != null)
                            previousRun = (Run)previousParagraph.Inlines.FirstInline;

                        /*do
                        {
                            previousParagraph = (Paragraph)previousParagraph.PreviousBlock;

                            if (previousParagraph != null)
                                previousRun = (Run)previousParagraph.Inlines.FirstInline;

                            if (previousRun != null)
                            {
                                if (previousRun.Text.Contains("}"))
                                    break;
                                else if (previousRun.Text.Contains("{"))
                                    break;
                            }

                        } while (previousParagraph != null);*/

                        if (run != null && previousRun != null)
                        {
                            int tabs = 0;
                            foreach (Char c in previousRun.Text)
                            {
                                if (c != '\t')
                                    break;
                                tabs++;
                            }

                            if (true && run.Text.Length > 0)
                            {
                                String lastChar = run.Text.Substring(run.Text.Length - 1);

                                String chr = run.Text.TrimStart(new char[] { '\t', ' ' });

                                if (chr.StartsWith("}"))
                                {
                                    if (previousRun.Text.Contains("{"))
                                    {
                                        run.ContentStart.DeleteTextInRun(1);
                                    }
                                    else
                                    {
                                        int currentLineTabs = 0;
                                        foreach (Char c in run.Text)
                                        {
                                            if (c != '\t')
                                                break;
                                            currentLineTabs++;
                                        }

                                        if (currentLineTabs >= tabs && tabs > 0)
                                            run.ContentStart.DeleteTextInRun(currentLineTabs - tabs + 1);
                                        else if (tabs > currentLineTabs)
                                            run.ContentStart.InsertTextInRun(new String('\t', tabs - currentLineTabs - 1));
                                    }
                                }
                            }

                        }
                    }
                }
            }

            this.TextChanged += this.TextChangedEventHandler;
        }

        #endregion

        System.Timers.Timer _timer;

        Boolean autoIndents = false;

        public Boolean AutoIndents
        {
            get { return autoIndents; }
            set { autoIndents = value; }
        }

        Boolean textWrap = false;

        public Boolean TextWrap
        {
            get { return textWrap; }
            set { textWrap = value; }
        }

        private static readonly RoutedUICommand[] _formattingCommands = new RoutedUICommand[]
            {
                EditingCommands.ToggleBold,
                EditingCommands.ToggleItalic,
                EditingCommands.ToggleUnderline,
                EditingCommands.ToggleSubscript,
                EditingCommands.ToggleSuperscript,
                EditingCommands.IncreaseFontSize,
                EditingCommands.DecreaseFontSize,
                EditingCommands.ToggleBullets,
                EditingCommands.ToggleNumbering,
            };

        CodeColorizer.Language.ILanguage lang = null;

        public CodeColorizer.Language.ILanguage Lang
        {
            get { return lang; }
            set 
            { 
                lang = value;
                UpdateStyle();
            }
        }
        CodeColorizer.Theme.ITheme theme = null;

        public CodeColorizer.Theme.ITheme Theme
        {
            get { return theme; }
            set 
            { 
                theme = value;
                UpdateStyle();
            }
        }
    }
}