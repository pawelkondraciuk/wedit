using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace wEdit
{
    internal class MyContextMenu : ContextMenu
    {
        public MyContextMenu(SyntaxRTB myRichTextBox) : base()
        {
            _myRichTextBox = myRichTextBox;
        }

        protected override void OnOpened(RoutedEventArgs e)
        {
            this.Items.Clear();
            this.AddClipboardMenuItems();            
        }

        private void AddClipboardMenuItems()
        {
            MenuItem menuItem;

            menuItem = new MenuItem();
            menuItem.Header = ApplicationCommands.Cut.Text;
            menuItem.CommandTarget = _myRichTextBox;
            menuItem.Command = ApplicationCommands.Cut;
            this.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = ApplicationCommands.Copy.Text;
            menuItem.CommandTarget = _myRichTextBox;
            menuItem.Command = ApplicationCommands.Copy;
            this.Items.Add(menuItem);

            menuItem = new MenuItem();
            menuItem.Header = ApplicationCommands.Paste.Text;
            menuItem.CommandTarget = _myRichTextBox;
            menuItem.Command = ApplicationCommands.Paste;
            this.Items.Add(menuItem);
        }

        private void AddSeperator()
        {
            this.Items.Add(new Separator());
        }

        private TextPointer GetMousePosition()
        {
            Point mousePoint = Mouse.GetPosition(_myRichTextBox);
            return _myRichTextBox.GetPositionFromPoint(mousePoint, /*snapToText*/true);
        }

        private readonly SyntaxRTB _myRichTextBox;
    }
}