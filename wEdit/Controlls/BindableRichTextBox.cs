﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace wEdit
{
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    public class BindableRichTextBox : SyntaxRTB
    {
        public static readonly DependencyProperty DocumentProperty = DependencyProperty.Register("Document", typeof(FlowDocument), typeof(BindableRichTextBox), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnDocumentChanged)));
        public static readonly DependencyProperty ThemeProperty = DependencyProperty.Register("Theme", typeof(CodeColorizer.Theme.ITheme), typeof(BindableRichTextBox), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnThemeChanged)));
        public static readonly DependencyProperty LangProperty = DependencyProperty.Register("Lang", typeof(CodeColorizer.Language.ILanguage), typeof(BindableRichTextBox), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnLanguageChanged)));
        public static readonly DependencyProperty AutoIndentsProperty = DependencyProperty.Register("AutoIndents", typeof(Boolean), typeof(BindableRichTextBox), new FrameworkPropertyMetadata(new PropertyChangedCallback(OnAutoIndentsChanged)));
        public static readonly DependencyProperty TextWrapProperty = DependencyProperty.Register("TextWrap", typeof(Boolean), typeof(BindableRichTextBox), new FrameworkPropertyMetadata(new PropertyChangedCallback(OnTextWrapChanged)));
        
        public new FlowDocument Document
        {
            get
            {
                return (FlowDocument)this.GetValue(DocumentProperty);
            }

            set
            {
                this.SetValue(DocumentProperty, value);
            }
        }

        public new CodeColorizer.Theme.ITheme Theme
        {
            get
            {
                return (CodeColorizer.Theme.ITheme)this.GetValue(ThemeProperty);
            }

            set
            {
                this.SetValue(ThemeProperty, value);
            }
        }

        public new CodeColorizer.Language.ILanguage Lang
        {
            get
            {
                return (CodeColorizer.Language.ILanguage)this.GetValue(LangProperty);
            }

            set
            {
                this.SetValue(LangProperty, value);
            }
        }

        public Boolean AutoIndents
        {
            get
            {
                return (Boolean)this.GetValue(AutoIndentsProperty);
            }

            set
            {
                this.SetValue(AutoIndentsProperty, value);
            }
        }

        public Boolean TextWrap
        {
            get
            {
                return (Boolean)this.GetValue(TextWrapProperty);
            }

            set
            {
                this.SetValue(TextWrapProperty, value);
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            TextChanged += BindableRichTextBox_TextChanged;
            base.OnInitialized(e);
        }

        void BindableRichTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var binding = BindingOperations.GetBinding(this, DocumentProperty);

            if (binding.UpdateSourceTrigger == UpdateSourceTrigger.PropertyChanged)
            {
                BindingOperations.GetBindingExpression(this, DocumentProperty).UpdateSource();
            }
        }

        public static void OnDocumentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            BindableRichTextBox rtb = (BindableRichTextBox)obj;

            FlowDocument flow = (FlowDocument)args.NewValue;

            if (flow == null)
                flow = new FlowDocument();

            rtb.setDocument(flow);
        }

        public static void OnThemeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            SyntaxRTB rtb = (BindableRichTextBox)obj;
            rtb.Theme = (CodeColorizer.Theme.ITheme)args.NewValue;
        }

        public static void OnLanguageChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            SyntaxRTB rtb = (BindableRichTextBox)obj;
            rtb.Lang = (CodeColorizer.Language.ILanguage)args.NewValue;
        }

        public static void OnAutoIndentsChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            SyntaxRTB rtb = (BindableRichTextBox)obj;
            rtb.AutoIndents = (Boolean)args.NewValue;
        }

        public static void OnTextWrapChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            SyntaxRTB rtb = (BindableRichTextBox)obj;
            if ((Boolean)args.NewValue == false)
                rtb.Document.PageWidth = 1000;
            else
                rtb.Document.PageWidth = rtb.Width;
        }
    }
}
